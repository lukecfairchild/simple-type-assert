const Type = require('../src');

const options = {
	port    : 1234,
	address : '0.0.0.0'
}

Type.assert([options.ssl], [undefined, Boolean]);

//process.exit(0);






class Class {}

var classInstance = new Class();

var input = {
	Object : {
		Function : () => {},
		null   : null,
		Symbol : Symbol('Symbol'),
		Array  : [
			{
				Boolean : false,
				Array   : [
					{
						String      : '',
						StringArray : ['', '']
					},
					{
						String      : '',
						StringArray : ['', '']
					}
				]
			},
			null
		]
	},
	String : '',
	Number : 42,
	NaN    : [
		classInstance,
		null,
		'',
		() => {},
		{},
		[],
		Symbol('Symbol')
	]
};

var x = process.hrtime()
Type.assert(input, {
	Object : {
		Function : Function,
		null     : null,
		Symbol   : Symbol,
		Array    : [
			{
				Boolean : Boolean,
				Array   : [
					{
						StringArray : [String],
						String      : String
					}
				]
			},
			null
		]
	},
	String : String,
	Number : Number,
	NaN    : [NaN]
});
